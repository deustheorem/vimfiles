execute pathogen#infect()
filetype plugin indent on       " Automatically detect file types

scriptencoding utf-8

set shortmess+=filmnrxoOtT      " Abbrev. of messages (avoids 'hit enter')
set viewoptions=folds,options,cursor,unix,slash " Better unix/windows compatibility
set virtualedit=onemore " Allow for cursor beyond last character

set number              " Display line numbers
set nuw=6               " Make line numbers 6 wide
set ignorecase          " Case insensitive search
set smartcase           " Case sensitive when uc present
set title               " Change terminal title
set nobackup            " Disable the creation of backup files
set mouse=a             " Enable mouse, now I must slap myself for turning this on
set spell               " Enable spell checker
set history=1000        " Increase command history
set undolevels=1000     " Increase undo history
set cursorline          " Highlight line that cursor is on
set scrolljump=5        " Lines to scroll when cursor leaves screen
set scrolloff=3         " Minimum lines to keep above and below cursor
set hlsearch            " Highlight search terms
set incsearch           " Find as you type search
set showmatch           " Show matching brackets/parenthesis
set linespace=0         " No extra spaces between rows
set foldenable          " Auto fold code
set gdefault            " The /g flag on :s substitutions by default
set pastetoggle=<F2>    " Toggle paste mode
set nowrap              " No wrap long lines

set wildmenu            " Show list instead of just completing
set wildmode=list:longest,full  " Command <Tab> completion, list matches, then longest common part, then all.

set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.   " Highlight problematic whitespace

" Remove trailing whitespaces and ^M chars
autocmd FileType c,cpp,java,php,js,python,twig,xml,yml autocmd BufWritePre <buffer> :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")'))

if has('cmdline_info')
        set ruler
        set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%)
        set showcmd
endif

if has('statusline')
        set laststatus=2                                "
        set statusline=%<%f\                            " Filename
        set statusline+=%w%h%m%r                        " Options
        set statusline+=%{fugitive#statusline()}        " Git Hotness
        set statusline+=\ [%{&ff}/%Y]                   " Filetype
        set statusline+=\ [%{getcwd()}]                 " Current dir
        "set statusline+=\ [A=\%03.3b/H=\%02.2B] " ASCII / Hexadecimal value
        "of char
        set statusline+=%=%-14.(%l,%c%V%)\ %p%%         " Right aligned file nav info
endif


if &t_Co >= 256 || has("gui_running")
        colorscheme mustang
endif

if &t_Co >= 2 || has("gui_running")
        syntax on
        set background=dark
endif
